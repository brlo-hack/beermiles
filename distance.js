function toRadian (x) {
  return (Math.PI / 180) * x
}
// implementation of the Haversine distance formula https://en.wikipedia.org/wiki/Haversine_formula to calculate the distance between 2 lat/lng in kilometers
// NOTE: the distance returned can be incorrect by up to 0.5%
// Read the wikipedia article for more information
function distance (latitude1, longitude1, latitude2, longitude2) {
  // https://en.wikipedia.org/wiki/Earth_radius
  const MeanEarthRadius = 6371
  const rLat1 = toRadian(latitude1)
  const rLng1 = toRadian(longitude1)
  const rLat2 = toRadian(latitude2)
  const rLng2 = toRadian(longitude2)
  const lhs = Math.pow(Math.sin((rLat2 - rLat1) / 2), 2)
  const rhs = Math.cos(rLat1) * Math.cos(rLat2) * Math.pow(Math.sin((rLng2 - rLng1) / 2), 2)
  const angularDistance = 2 * Math.asin(Math.sqrt(lhs + rhs))
  return angularDistance * MeanEarthRadius
}

module.exports = {
  distance
}
