'use strict';

const express = require('express');
const rp = require('request-promise');
const app = express();
const distance = require('./distance').distance;
const { encrypt, decrypt } = require('./encryption');
const { authenticateUrl, authorizeUrl, checkinsUrl } = require('./untappd')

app.set('view engine', 'jade');

const host = process.env.NODE_ENV === 'production' ? 'https://beermiles.herokuapp.com' : 'http://localhost:3000';
//
// Initial page redirecting to untappd
app.get('/auth', (req, res) => {
  console.log(authenticateUrl())
  res.redirect(authenticateUrl());
});

// Callback service parsing the authorization token and asking for the access token

app.get('/callback', (req, res) => {
  const code = req.query.code;
  const url = authorizeUrl(code);
  console.log("authorizeUrl", url);
  var options = {
    uri: url,
    json: true 
  };

  rp(options)
    .then(function (apiResponse) {
      console.log('Response', apiResponse);
      const accessToken = apiResponse.response.access_token;
      console.log("AccessToken", accessToken)
      const userUrl = host + '/miles/' + encrypt(accessToken);
      res.redirect(userUrl)
    })
    .catch(function (err) {
      console.error('Error getting authorizeurl', err);
    });
})

app.get('/success', (req, res) => {
  res.send('sucesses');
});


function withLocation(checkin) {
  return (checkin.brewery.location.lat !== 0) && (checkin.brewery.location.lng !== 0);
}

function beers(apiResponse) {
  const fernsehturm = { lat: 52.520645, lng: 13.409779 }
  const checkins = apiResponse.response.checkins.items
  return checkins.filter(withLocation).map(c => {
    const breweryLocation = c.brewery.location;
    const venueLocation = c.venue.location || fernsehturm;
    const dist = distance(venueLocation.lat, venueLocation.lng, breweryLocation.lat, breweryLocation.lng)
    return {
      beerName: c.beer.beer_name,
      beerLabel: c.beer.beer_label,
      brewery: c.brewery.brewery_name,
      venueName: c.venue.venue_name,
      breweryCity: c.brewery.location.brewery_city,
      distance: dist
    }
  })
}

function inBreweryBeers(beers){
  beers.map(x => console.log(x.distance)) 
  return beers.filter((x) => x.distance <= 0.15);
}

function farAwayBeers(beers){
  beers.map(x => console.log(x.distance)) 
  return beers.filter((x) => x.distance > 1000).sort(byDistance);
}
function byDistance(a, b) {
  if (a.distance < b.distance) {
    return 1;
  }
  if (a.distance > b.distance) {
    return -1;
  }
  // a must be equal to b
  return 0;
}

app.get('/miles/:userId', (req, res) => {
  const accessToken = decrypt(req.params.userId)
  console.log("accesstoken", accessToken)
  var options = {
    uri: checkinsUrl(accessToken),
    json: true 
  };

  rp(options)
    .then(function (apiResponse) {
      console.log('Response', apiResponse);
      const bs = beers(apiResponse);
      const average = bs.reduce((a, x) => a += x.distance, 0) / bs.length;
      const breweryBeers = inBreweryBeers(bs);
      const farBeers = farAwayBeers(bs);
      console.log(breweryBeers)
      const data = {
        beers: bs,
        hyperLocalBeers: inBreweryBeers(bs),
        farAwayBeers: farBeers,
        farBeer: farBeers[0],
        average: parseInt(average, 10).toLocaleString()
      };
      res.render('miles', data);
    })
    .catch(function (err) {
      console.error('Error getting authorizeurl', err);
    });
});

app.get('/', (req, res) => {
  res.render('home.jade', {})
});

const port = process.env.PORT || 3000


console.log('Using host %s', host)
console.log('Using port %d', port)
app.listen(port, function () {
  console.log('Started beermiles on port %d', port)
})
