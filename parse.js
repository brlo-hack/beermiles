const doc = require('./checkins.json')
const distance = require('./distance').distance;

const checkins = doc.response.checkins.items
console.log(checkins.length)
checkins.forEach(c => {
  // console.log("Location:", c.user.location)
  console.log("Beer:", c.beer.beer_name)
  console.log("Brewery:", c.brewery.brewery_name)
  console.log("Venue:", c.venue.venue_name)
  console.log("Brewery location", c.brewery.location.brewery_city)
  // console.log("Location:", c.venue.location)

  const breweryLocation = c.brewery.location;
  const venueLocation = c.venue.location;
  if (venueLocation) {
    console.log("Distance: ", distance(venueLocation.lat, venueLocation.lng, breweryLocation.lat, breweryLocation.lng))
  }
})

