const clientId = process.env.UNTAPPD_CLIENT_ID;
const clientSecret = process.env.UNTAPPD_CLIENT_SECRET;

const host = process.env.NODE_ENV === 'production' ? 'https://beermiles.herokuapp.com' : 'http://localhost:3000';

function redirectUri() {
  return host + '/callback';
}

function authenticateUrl() {
  const redirectUrl = encodeURIComponent(redirectUri());
  return 'https://untappd.com/oauth/authenticate/?client_id=' + clientId + '&response_type=code&redirect_url=' + redirectUrl;
}

function authorizeUrl(code) {
  const redirectUrl = encodeURIComponent(redirectUri());
  return 'https://untappd.com/oauth/authorize/?client_id=' + clientId + '&client_secret='+ clientSecret + '&response_type=code&redirect_url=' + redirectUrl + '&code=' + code;
}

function checkinsUrl(accessToken) {
  return 'https://api.untappd.com/v4/user/checkins?limit=50&access_token=' + accessToken;
}

module.exports = {
  authorizeUrl,
  authenticateUrl,
  checkinsUrl
};
